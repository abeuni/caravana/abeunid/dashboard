
export interface IntrospectionResultData {
        __schema: {
          types: {
            kind: string;
            name: string;
            possibleTypes: {
              name: string;
            }[];
          }[];
        };
      }
const result: IntrospectionResultData = {
  __schema: {
    types: [
      {
        kind: 'UNION',
        name: 'EventEntity',
        possibleTypes: [
          {
            name: 'Person'
          },
          {
            name: 'Card'
          },
          {
            name: 'Station'
          },
          {
            name: 'Room'
          }
        ]
      },
      {
        kind: 'UNION',
        name: 'EventData',
        possibleTypes: [
          {
            name: 'EventPersonAssociatedData'
          },
          {
            name: 'EventStationCardReadData'
          },
          {
            name: 'EventPersonUpdatedData'
          }
        ]
      }
    ]
  }
}
export default result
