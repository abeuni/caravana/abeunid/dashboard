import Vue from 'vue'
import Router from 'vue-router'
import { store } from './store'

Vue.use(Router)

export const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'root',
      redirect: { name: 'formulary' }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue'),
      meta: {
        redirect: { auth: { name: 'dashboard' } },
        noBack: true
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import('./views/Dashboard.vue'),
      meta: {
        redirect: {
          noAuth: { name: 'login' },
          auth: { name: 'formulary' }
        },
        noBack: true
      },
      children: [
        {
          path: '/formulary',
          name: 'formulary',
          component: () => import('./views/dashboard/Formulary.vue'),
          meta: {
            redirect: { noAuth: { name: 'login' } },
            noBack: true
          }
        },
        {
          path: '/users',
          name: 'users',
          component: () => import('./views/dashboard/Users.vue')
        },
        {
          path: '/user/:id',
          name: 'user',
          component: () => import('./views/dashboard/User.vue')
        },
        {
          path: '/persons',
          name: 'persons',
          component: () => import('./views/dashboard/Persons.vue')
        },
        {
          path: '/scanner',
          name: 'scanner',
          component: () => import('./views/dashboard/Scanner.vue')
        },
        {
          path: '/person/:id',
          name: 'person',
          component: () => import('./views/dashboard/Person.vue')
        },
        {
          path: '/cards',
          name: 'cards',
          component: () => import('./views/dashboard/Cards.vue')
        },
        {
          path: '/card/:id',
          name: 'card',
          component: () => import('./views/dashboard/Card.vue')
        },
        {
          path: '/stations',
          name: 'stations',
          component: () => import('./views/dashboard/Stations.vue')
        },
        {
          path: '/station/:id',
          name: 'station',
          component: () => import('./views/dashboard/Station.vue')
        },
        {
          path: '/caravans',
          name: 'caravans',
          component: () => import('./views/dashboard/Caravans.vue')
        },
        {
          path: '/metrics',
          name: 'metrics',
          component: () => import('./views/dashboard/Metrics.vue'),
          meta: {
            redirect: { noAuth: { name: 'login' } },
            noBack: true
          }
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  const { redirect } = to.meta

  if (redirect) {
    // redirect if authenticated
    if (redirect.auth && !!store.getters['auth/token']) next(redirect.auth)

    // redirect if not authenticated
    if (redirect.noAuth && !store.getters['auth/token']) next(redirect.noAuth)
  }

  next()
})
