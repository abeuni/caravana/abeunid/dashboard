import { Module } from 'vuex'

interface MachineState {
  id: string | null,
  name: string | null
}

export const machine: Module<MachineState, any> = {
  state: {
    id: null,
    name: null
  },
  actions: {

  }
}
