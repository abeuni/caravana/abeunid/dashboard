import { Module } from 'vuex'
import LoginQuery from '@/gql/LoginQuery.gql'
import UserQuery from '@/gql/UserQuery.gql'
import { apolloClient } from '@/vue-apollo'
import * as jwt from 'jsonwebtoken'
import * as Cookie from 'js-cookie'

export interface User {
  id: string,
  email: string,
  password?: string,
  roles: string[]
}

interface AuthState {
  user: User | null
}

export const auth: Module<AuthState, any> = {
  namespaced: true,
  state: {
    user: null
  },
  getters: {
    token () {
      return Cookie.get('authorization')
    },
    user (state: AuthState) {
      return state.user
    }
  },
  mutations: {
    dropToken () {
      Cookie.remove('authorization')
    },
    setUser (state: AuthState, user: User | null) {
      state.user = user
    }
  },
  actions: {
    async login ({ commit }: any, variables: {email: string, password: string}) {
      const result = (await apolloClient.query({
        query: LoginQuery,
        variables
      }))

      const token = result.data.login

      if (token) {
        const payload = jwt.decode(token)

        if (payload) {
          const user: User = (await apolloClient.query({
            query: UserQuery,
            variables: {
              id: payload.sub
            }
          })).data.user

          commit('setUser', user)
        }
      }
    },
    async logout ({ commit }: any) {
      commit('setUser', null)
      commit('dropToken')
    }
  }
}
