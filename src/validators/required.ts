export default function (name: string) {
  return (v: string | null) => !!v || name + ' é obrigatório!'
}
