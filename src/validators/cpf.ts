export default function (msg : string | null) {
  const invalidMsg = msg || 'Invalid CPF'

  return (v : string | null) => {
    const n = v && v.replace(/[^0-9]/g, '').split('').map((d) => parseInt(d))

    if (!n) {
      return invalidMsg
    }

    if (!v || n.length !== 11) {
      return invalidMsg
    }

    let sum = 0

    for (let i = 0; i < 9; i++) sum += (10 - i) * n[i]

    const firstDigit = ((sum * 10) % 11) % 10

    sum = 0

    for (let i = 0; i < 10; i++) sum += (11 - i) * n[i]

    const secondDigit = ((sum * 10) % 11) % 10

    return (n[9] === firstDigit && n[10] === secondDigit) || invalidMsg
  }
}
