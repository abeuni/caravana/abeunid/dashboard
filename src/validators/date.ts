export default function (msg : string | null) {
  const invalidMsg = msg || 'Invalid Date'
  return (v : string | null) => ((!!v && v.length === 10) || invalidMsg)
}
