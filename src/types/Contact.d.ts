export interface IContact {
  phoneNumber: string | null,
  emergencyPhoneNumber: string | null,
  emergencyContactName: string | null
}
