export interface IAddress {
  street: string | null,
  cep: string | null,
  number: string | null,
  complement: string | null,
}
