export interface IEvent {
  entityType: string;
  type: string;
  relevant: string[];
  entity: any;
  data: any;
}
