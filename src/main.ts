import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue'
import App from './App.vue'
import { router } from './router'
import './registerServiceWorker'
import vuetify from './plugins/vuetify'
import { apolloProvider } from './vue-apollo'
import { store } from './store'
import VueTheMask from 'vue-the-mask'

Vue.config.productionTip = false

Vue.use(VueTheMask)

new Vue({
  router,
  store,
  vuetify,
  apolloProvider: apolloProvider,
  render: h => h(App)
}).$mount('#app')
