import { DateTime } from 'luxon'

export function ISOtoDate (iso: string): string {
  const date = iso.split('T')[0]
  return DateTime.fromISO(date).toFormat('dd/MM/yyyy')
}

export function ISOtoAge (iso: string): number {
  const date = iso.split('T')[0]
  const birthDate = DateTime.fromISO(date)

  return Math.floor(-birthDate.diffNow('years').years)
}
